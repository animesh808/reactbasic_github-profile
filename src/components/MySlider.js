import React, {Component} from 'react';
import Slider from 'react-slick';

class MySlider extends Component {

  componentDidMount(){
    console.log("mounted");
  }

  render(){
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <Slider {...settings}>
        <div>
          <h3>{this.props.profile.name}</h3>
        </div>
        <div>
          <h3>{this.props.profile.public_email}</h3>
        </div>
        <div>
          <h3>{this.props.profile.organization}</h3>
        </div>
        <div>
          <h3>{this.props.profile.job_title}</h3>
        </div>
        <div>
          <h3>{this.props.profile.location}</h3>
        </div>
        <div>
          <img className="center" src={this.props.profile.avatar_url} />
        </div>
      </Slider>
    );
  };
};

export default MySlider;
