import React, {Component} from 'react';
import {Form, Button} from 'react-bootstrap';

class Profile extends Component {

  constructor(props){
    super(props);
    this.state = {
      profileData : this.props.profile,
      isEdit : false,
      error: false
    }
  }

  componentDidMount(){
  }

  componentWillReceiveProps(nextProps){
    this.setState({
      profileData : nextProps.profile
    })
  }

  updateValue(type, event){
    var profileDataCopy = JSON.parse(JSON.stringify(this.state.profileData));
    profileDataCopy[type] = event.target.value
    this.setState({profileData : profileDataCopy})
  }


  saveProfile(){
    var error = false;
    var fieldsToCheck = ['name', 'public_email', 'job_title', 'organization', 'location']
    fieldsToCheck.forEach(function(item, i){
      if (this.state.profileData[item] === '') {
        error = true;
      };
    }.bind(this));
    if (!error) {
      this.props.saveProfile(this.state.profileData)
    }
    this.setState({error})

  }

  render(){
    return(
      <div className="container">
        <br />
        <Button variant="primary" onClick={() => this.setState({isEdit : !this.state.isEdit})}>
          {!this.state.isEdit ? 'Edit' : 'Cancel Edit'}
        </Button>
        <br /> <hr />

        {this.state.isEdit ?
          <Form id="profileForm">
            <Form.Group>
              <Form.Label>Name:</Form.Label>
              <Form.Control className={this.state.error && this.state.profileData.name === '' ? 'red-border' : ''} type="text" name="name" placeholder="Normal text" value={this.state.profileData.name} onChange={this.updateValue.bind(this, 'name')} />
              <br />
              <Form.Label>Email:</Form.Label>
              <Form.Control className={this.state.error && this.state.profileData.public_email === '' ? 'red-border' : ''} type="email" name="email" placeholder="Normal text" value={this.state.profileData.public_email} onChange={this.updateValue.bind(this, 'public_email')} />
              <br />
              <Form.Label>Organization:</Form.Label>
              <Form.Control type="text" className={this.state.error && this.state.profileData.organization === '' ? 'red-border' : ''} name="org" placeholder="Normal text" value={this.state.profileData.organization} onChange={this.updateValue.bind(this, 'organization')} />
              <br />
              <Form.Label>Job Title:</Form.Label>
              <Form.Control type="text" className={this.state.error && this.state.profileData.job_title === '' ? 'red-border' : ''} name="position" placeholder="Normal text" value={this.state.profileData.job_title} onChange={this.updateValue.bind(this, 'job_title')} />
              <br />
              <Form.Label>Location:</Form.Label>
              <Form.Control type="text" className={this.state.error && this.state.profileData.location === '' ? 'red-border' : ''} name="location" placeholder="Normal text" value={this.state.profileData.location} onChange={this.updateValue.bind(this, 'location')} />
              <br />
              <Button variant="info" onClick={this.saveProfile.bind(this)} >Submit</Button>
            </Form.Group>
          </Form>

          :
          <div>
            <strong>Name: </strong>{this.state.profileData.name} <br />
            <strong>Email: </strong>{this.state.profileData.public_email} <br />
            <strong>Organization: </strong>{this.state.profileData.organization} <br />
            <strong>Job Title: </strong>{this.state.profileData.job_title} <br />
            <strong>Location: </strong>{this.state.profileData.location}
          </div>

        }


      </div>
    )
  };
};

export default Profile;
