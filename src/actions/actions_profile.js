export const PROFILE_FETCHED = 'PROFILE_FETCHED';
export const PROFILE_EDITED = 'PROFILE_EDITED';

export function fetchProfile(){
  return (dispatch) => {
    let header = new Headers({"Content-Type":"application/json", "PRIVATE-TOKEN":"K1wEkQZwhPiDyB7qSfvC"})
    return fetch("https://gitlab.com/api/v4/users/1166351", {
      method: 'GET',
      headers: header
    })
    .then(response => response.json())
    .then(json => {
      dispatch(loadProfile(json))
    })
    .catch(error => console.log(error));
  }
}

export function saveProfile(profile){
  return (dispatch) => {
    let header = new Headers({"Content-Type":"application/json", "PRIVATE-TOKEN":"K1wEkQZwhPiDyB7qSfvC"})
    return fetch("https://gitlab.com/api/v4/users/1166351", {
      method: 'PUT',
      headers: header,
      body: JSON.stringify(profile)
    })
    .then(response => response.json())
    .then(json => {
      dispatch(loadProfile(json))
    })
    .catch(error => console.log(error));
  }
}

export function loadProfile(results){
  return {
    type: PROFILE_FETCHED,
    payload: results
  }
}
