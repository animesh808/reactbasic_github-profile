import { connect } from 'react-redux';
import { fetchProfile, saveProfile } from '../actions/actions_profile'

import ProfileComponent from '../components/Profile';

const mapStateToProps = state => {
  return {
    profile : state.profile
  };
}

const mapDispatchToProfile = (dispatch, ownProps) => {
  return {
    fetchProfile : () => {
      dispatch(fetchProfile());
    },
    saveProfile : (profile) => {
      dispatch(saveProfile(profile));
    }
  }
}

const Profile = connect(
  mapStateToProps,
  mapDispatchToProfile
)(ProfileComponent);

export default Profile;
