import { connect } from 'react-redux';
import { fetchProfile } from '../actions/actions_profile'

import AppComponent from '../components/App';

const mapDispatchToProfile = (dispatch, ownProps) => {
  return {
    fetchProfile : () => {
      dispatch(fetchProfile());
    }
  }
}

const Profile = connect(
  false,
  mapDispatchToProfile
)(AppComponent);

export default Profile;
